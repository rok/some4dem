\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{some4dem}
% \ProvidesClass[10pt,a4paper,twoside=semi,twocolumn,mpinclude=false,footinclude=false]{scrartcl}

%% use base class
\LoadClass[paper=a4,11pt]{scrreprt}
\RequirePackage{scrlayer-scrpage}

\RequirePackage[left=2.5cm,right=2.5cm,top=3.0cm,bottom=3.0cm]{geometry}
%% fonts
\RequirePackage{helvet}
\RequirePackage[cmintegrals,cmbraces]{newtxmath}
\RequirePackage[lining]{ebgaramond}
\RequirePackage{ebgaramond-maths}
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}
%% literature
\RequirePackage[backend=biber,style=authoryear,sorting=nyt,maxnames=4]{biblatex}
%% graphics
\RequirePackage{tikz}
%% headers
% \RequirePackage{fancyhdr}
%% new command/environment definition
\RequirePackage{xparse}
%% general and default colours
\RequirePackage{xcolor}
%% spaces after commands
\RequirePackage{xspace}
%% improved font typesetting
\RequirePackage{microtype}
%% better tables
\RequirePackage{booktabs}

%\typearea{13}
% \setlength{\columnsep}{1.5cc}

\DeclareFieldFormat
  [article,inbook,incollection,inproceedings,patent,thesis,unpublished,misc]
  {title}{\mkbibquote{{\textit{#1}}}}
\renewcommand*{\bibfont}{\normalsize}
\addtolength{\bibitemsep}{1em}

\defbibheading{bibliography}[\bibname]{%
  \chapter*{References}%
  \markboth{#1}{#1}}

\renewcommand{\bibname}{References}

%%
%% titlepage options
%%
\makeatletter
\newcommand*{\@affiliation}{}
\newcommand*{\affiliation}[1]{\renewcommand*{\@affiliation}{#1}}
\makeatother

%%
%% color definition
%%
\definecolor{sectioncolor}{HTML}{2f5496}
\newcommand{\sectioncolor}{\color{sectioncolor}}
% \colorlet{spotcolor}{SkyBlue3!75!black}
% \newcommand{\spotcolor}{\color{spotcolor}}

%%
%% KOMA options
%%
\addtokomafont{title}{\sffamily\sectioncolor}
\addtokomafont{paragraph}{\sectioncolor}
\addtokomafont{chapter}{\sectioncolor}
\addtokomafont{section}{\sectioncolor}
\addtokomafont{subsection}{\sectioncolor}
\addtokomafont{subsubsection}{\sectioncolor}
\addtokomafont{descriptionlabel}{\sectioncolor}
\addtokomafont{chapterentry}{\rmfamily}
\setkomafont{caption}{}
\setkomafont{captionlabel}{}
\KOMAoptions{numbers=noenddot}

%% 
%% page header/footer
%%
% \theoremstyle{plain}
% \theoremheaderfont{\bfseries \upshape}
% \newtheorem{define}{Definition}[section]
% \newtheorem{remark}[define]{Remark}

% \setkomafont{author}{\normalsize}
% \setkomafont{date}{\normalsize}

%%
%% title within first column
%% - comment out to have title above columns
%%
\makeatletter
\renewcommand{\maketitle}{
  \pagestyle{empty}
  \tikz[remember picture,overlay] \node[inner sep=0pt] at (current page.center){\includegraphics[width=\paperwidth,height=\paperheight]{cover.jpg}};
  % \tikz[remember picture,overlay] \node[inner sep=0pt] at (current page.north west){\tikz \draw[red] (0,0) -- (5cm,0cm) -- (5cm,-60cm);};
  \tikz[remember picture,overlay] \node[inner sep=0pt,anchor=north west] at (-0.55cm,-4.25cm){\Huge\color{white}\sffamily\bfseries{\@title}};
  \tikz[remember picture,overlay] \node[inner sep=0pt,anchor=north west] at (-0.6cm,-9.75cm){\LARGE\color{white}\sffamily{\@author}};
  \tikz[remember picture,overlay] \node[inner sep=0pt,anchor=north west] at (-0.6cm,-10.50cm){\color{white}\sffamily{\@affiliation}};
  \tikz[remember picture,overlay] \node[inner sep=0pt,anchor=north west] at (-0.75cm,-12.50cm){\color{white}\sffamily{\today}};
  % \null
  % \vskip 4.0cm
  % \parbox{0.96\linewidth}{\Huge\color{white}\sffamily\bfseries\@title\par}
  % \vskip 1.0em
  % \parbox{0.96\linewidth}{\centering{{\large \@subtitle\par}}}
  % \vskip 1.5em
  % \parbox{0.96\linewidth}{%
  %   \centering
  %   {%
  %     \lineskip 0.75em
  %     \begin{tabular}[t]{c}
  %       \@author
  %     \end{tabular}\par
  %   }%
  % }%
  % \vskip 1.5em
  % \parbox{0.96\linewidth}{\centering{{\@date\par}}}
  % \vskip 2.0em
}
\makeatother

%%
%% headers
%%

% \makeatletter
%\pagestyle{plain}
% \pagestyle{fancy}
% \thispagestyle{plain}
% \fancyhf{}
% \fancyhf[HLE]{\footnotesize\thepage\hfill \@author}
% \fancyhf[HRO]{\footnotesize \@title\hfill\thepage}
% \fancyhf[FC]{\sffamily\thepage}
% \renewcommand{\headrulewidth}{0.4pt}
% \renewcommand{\footrulewidth}{0pt}
% \thispagestyle{empty}
% \fancyfoot[C]{\footnotesize\sffamily\thepage}
% \makeatother

% \cefoot[\sffamily\thepage]{\sffamily\thepage}
% \cofoot[\sffamily\thepage]{\sffamily\thepage}

%%
%% define different sections in report
%%

\pagenumbering{arabic}%

\makeatletter

\newcommand{\@pagenumberformat}{\small\sffamily\upshape\thepage}
  
\newcommand\frontmatter{%
  \cleardoublepage%
  \pagestyle{plain}%
  \@mainmatterfalse
  \renewcommand{\thechapter}{}%
  \cefoot[\@pagenumberformat]{\@pagenumberformat}%
  \cofoot[\@pagenumberformat]{\@pagenumberformat}%
} % roman

\newcommand\mainmatter{%
  \cleardoublepage%
  \pagestyle{plain}%
  \@mainmattertrue
  \renewcommand{\thechapter}{\arabic{chapter}}%
  \cefoot[\@pagenumberformat]{\@pagenumberformat}%
  \cofoot[\@pagenumberformat]{\@pagenumberformat}%
}

\newcommand\backmatter{%
  \if@openright
    \cleardoublepage
  \else
    \clearpage
  \fi
  \@mainmatterfalse
  \pagestyle{plain}%
  \renewcommand{\thechapter}{\alph{chapter}}%
  \cefoot[\@pagenumberformat]{\@pagenumberformat}%
  \cofoot[\@pagenumberformat]{\@pagenumberformat}%
}
  
\makeatother
